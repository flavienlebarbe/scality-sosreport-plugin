"""SOS Scality Plugin
author trevor dot benson at scality dot com
Current file revision should be obtained from
https://bitbucket.org/scality-ts/scality-sosreport-plugin
"""

from sos.plugins import Plugin, RedHatPlugin


class Scality(Plugin, RedHatPlugin):
    """Scality Ring system information
    """

    plugin_name = 'scality'
    profiles = ('storage',)
    option_list = [
        ("fastcgi", "enable FastCGI log collection", "fast", False),
        ("all_logs", "gather all logs and logrotated archives", "fast", False),
        ("log_days", "the number of days worth of logs to collect", "", "0"),
        ("ringsh_ringList", "get ringList from supervisor", "fast", False),
        ("ringsh_ringStatus", "get ringStatus from all rings", "fast", False),
        ("ringsh_ringStorage", "get ringStatus from all rings", "fast", False),
        ("ringsh_ringTasks", "get ringStatus from all rings", "fast", False),
        ("ringsh_configGet", "get configs from each node on all rings", "slow", False),
        ("ringsh_all", "run all ringsh commands", "slow", False),
    ]

    def setup(self):
        # Add the following config and log files to the copy_spec to be included
        self.add_copy_spec([
            "/etc/sagentd.yaml",
        ])

        if self.is_installed("scality-supervisor"):
            self.add_copy_spec([
                "/etc/scality-supervisor/confdb/*",
                "/etc/scality-supervisor/confv2.yaml",
                "/var/log/scality-supervisor/audittrail.log",
                "/var/log/scality-supervisor/auth.log",
                "/var/log/scality-supervisor/chordsupervisor.log",
                "/var/log/scality-supervisor/cluster.log",
                "/var/log/scality-supervisor/csr.log",
                "/var/log/scality-supervisor/dnsclient.log",
                "/var/log/scality-supervisor/memory.log",
                "/var/log/scality-supervisor/netscript.log",
                "/var/log/scality-supervisor/ssl.log",
                "/var/log/scality-supervisor/system.log",
            ])
            if int(self.get_option("log_days")) > 0:
                self.find_files_by_mtime("/var/log/scality-supervisor/")

        if self.is_installed("scality-sproxyd"):
            self.add_copy_spec([
                "/var/log/scality-sproxyd.log",
                "/var/log/httpd/access_log",
                "/var/log/httpd/error_log",
            ])
            if int(self.get_option("log_days")) > 0:
                self.find_files_by_mtime("/var/log/scality-sproxyd*/")
                self.find_files_by_mtime("/var/log/httpd/")

        if self.is_installed("scality-sindexd"):
            self.add_copy_spec([
                "/var/log/scality-sindexd.log",
            ])
            if int(self.get_option("log_days")) > 0:
                self.find_files_by_mtime("/var/log/scality-sindexd*/")

        if self.is_installed("scality-rest-connector"):
            self.add_copy_spec([
                "/etc/scality-rest-connector/confdb/*",
                "/etc/scality-rest-connector/*.conf",
                "/etc/scality-rest-connector/*.xml",
                "/var/log/scality-rest-connector/audittrail.log",
                "/var/log/scality-rest-connector/authbws.log",
                "/var/log/scality-rest-connector/auth.log",
                "/var/log/scality-rest-connector/chordapi.log",
                "/var/log/scality-rest-connector/chordbucket.log",
                "/var/log/scality-rest-connector/chorddb.log",
                "/var/log/scality-rest-connector/chordgc.log",
                "/var/log/scality-rest-connector/chord.log",
                "/var/log/scality-rest-connector/chordsplit.log",
                "/var/log/scality-rest-connector/chunkapi.log",
                "/var/log/scality-rest-connector/cluster.log",
                "/var/log/scality-rest-connector/chunkapi.log",
                "/var/log/scality-rest-connector/clusternode.log",
                "/var/log/scality-rest-connector/dnsclient.log",
                "/var/log/scality-rest-connector/gcache.log",
                "/var/log/scality-rest-connector/memcheck.log",
                "/var/log/scality-rest-connector/memory.log",
                "/var/log/scality-rest-connector/netscript.log",
                "/var/log/scality-rest-connector/ows.log",
                "/var/log/scality-rest-connector/pm.log",
                "/var/log/scality-rest-connector/restapi.log",
                "/var/log/scality-rest-connector/restclient.log",
                "/var/log/scality-rest-connector/slist.log",
                "/var/log/scality-rest-connector/ssl.log",
                "/var/log/scality-rest-connector/system.log",
                "/var/log/scality-rest-connector/tier1sync.log",
                "/var/log/httpd/access_log",
                "/var/log/httpd/error_log",
            ])
            if int(self.get_option("log_days")) > 0:
                self.find_files_by_mtime("/var/log/scality-rest-connector/")
                self.find_files_by_mtime("/var/log/httpd/")

        if self.is_installed("scality-node"):
            self.add_copy_spec([
                "/etc/scality-node*/nodes.conf",
                "/etc/scality-node*/*.conf",
                "/etc/scality-node*/*.xml",
                "/proc/diskstats",
                "/var/log/scality-node*/audittrail.log",
                "/var/log/scality-node*/auth.log",
                "/var/log/scality-node*/chordapi.log",
                "/var/log/scality-node*/chorddb.log",
                "/var/log/scality-node*/chordgc.log",
                "/var/log/scality-node*/chord.log",
                "/var/log/scality-node*/chordsplit.log",
                "/var/log/scality-node*/chunkapi.log",
                "/var/log/scality-node*/clusternode.log",
                "/var/log/scality-node*/dnsclient.log",
                "/var/log/scality-node*/memcheck.log",
                "/var/log/scality-node*/memory.log",
                "/var/log/scality-node*/netscript.log",
                "/var/log/scality-node*/ows.log",
                "/var/log/scality-node*/pm.log",
                "/var/log/scality-node*/restclient.log",
                "/var/log/scality-node*/slist.log",
                "/var/log/scality-node*/ssl.log",
                "/var/log/scality-node*/system.log",
                "/var/log/scality-node*/tier1sync.log",
                "/var/log/scality-srebuildd.log",
            ])
            if int(self.get_option("log_days")) > 0:
                self.find_files_by_mtime("/var/log/scality-node*")
                self.find_files_by_mtime("/var/log/scality-srebuildd*")

        if self.is_installed("scality-biziod"):
            self.add_copy_spec([
                "/etc/biziod/*",
                "/etc/bizstor*/confdb/*",
                "/proc/diskstats",
            ])

        if self.is_installed("scality-sfused"):
            self.add_copy_spec([
                "/var/log/scality-sfused.log",
            ])
            if int(self.get_option("log_days")) > 0:
                self.find_files_by_mtime("/var/log/scality-sfused*")

        if self.is_installed("python-scality"):
            # Need to add code for leveraging scality library for calls instead of ringsh
            pass

        if self.get_option("fastcgi"):
            self.add_copy_spec("/var/log/httpd/fastcgi/*")

        if self.get_option("all_logs"):
            self.add_copy_spec([
                "/var/log/scality*",
                "/var/log/httpd/*",
            ])

        if self.is_installed("scality-ringsh"):
            if self.get_option("ringsh_ringList"):
                self.get_ringsh_ringlist()

            if self.get_option("ringsh_ringStatus"):
                self.get_ringsh_ringlist()
                for ring in self.ringlist:
                    self.add_ringsh_supcmd_output(ring, "ringStatus")

            if self.get_option("ringsh_ringStorage"):
                self.get_ringsh_ringlist()
                for ring in self.ringlist:
                    self.add_ringsh_supcmd_output(ring, "ringStorage")

            if self.get_option("ringsh_ringTasks"):
                self.get_ringsh_ringlist()
                for ring in self.ringlist:
                    self.add_ringsh_supcmd_output(ring, "ringTasks")

            if self.get_option("ringsh_configGet"):
                self.get_ringsh_ringlist()
                for ring in self.ringlist:
                    self.add_ringsh_supcmd_output(ring, "ringConfigGet")
                    self.get_ringsh_nodelist(ring)
                    for node in self.nodelist:
                        self.get_ringsh_config(ring, node, "node")
                    self.get_ringsh_accessorlist(ring)
                    for accessor in self.accessorlist:
                        self.get_ringsh_config(ring, accessor, "accessor")

            if self.get_option("ringsh_all"):
                self.get_ringsh_ringlist()
                for ring in self.ringlist:
                    self.add_ringsh_supcmd_output(ring, "ringStatus")
                    self.add_ringsh_supcmd_output(ring, "ringStorage")
                    self.add_ringsh_supcmd_output(ring, "ringTasks")
                    self.add_ringsh_supcmd_output(ring, "ringConfigGet")
                    self.get_ringsh_nodelist(ring)
                    for node in self.nodelist:
                        self.get_ringsh_config(ring, node, "node")
                    self.get_ringsh_accessorlist(ring)
                    for accessor in self.accessorlist:
                        self.get_ringsh_config(ring, accessor, "accessor")

    def get_ringsh_ringlist(self):
        cmd = "ringsh supervisor ringList"
        self.add_cmd_output(cmd)
        result = self.call_ext_prog(cmd)
        self.ringlist = result['output'].splitlines()

    def add_ringsh_supcmd_output(self, ringname, command):
        cmd = "ringsh supervisor " + command + " " + ringname
        self.add_cmd_output(cmd)

    def get_ringsh_nodelist(self, ringname):
        self.nodelist = ([])
        cmd = "ringsh supervisor ringStatus " + ringname
        self.add_cmd_output(cmd)
        result = self.call_ext_prog(cmd)
        matching = [s for s in result['output'].splitlines() if "Node:" in s]
        for line in matching:
            fields = line.strip().split()
            self.nodelist.append(fields[1])
        return 0

    def get_ringsh_accessorlist(self, ringname):
        self.accessorlist = ([])
        cmd = "ringsh supervisor ringStatus " + ringname
        self.add_cmd_output(cmd)
        result = self.call_ext_prog(cmd)
        matching = [s for s in result['output'].splitlines() if "Connector:" in s]
        for line in matching:
            fields = line.strip().split()
            self.accessorlist.append(fields[1])
        return 0

    def get_ringsh_config(self, ringname, accessorname, nodetype):
        cmd = "ringsh -r " + ringname + " -u " + accessorname + " " + nodetype + " configGet"
        self.add_cmd_output(cmd)

    '''
    find_files_by_mtime() requires the path to the log directory. The value contained in log_days will be set to mtime
    (e.g. find / -type -f -mtime -${mtime}). If "scality.log_days" is called without an = sign (e.g. scality.log_days=5)
    then it will set the value to true and when converting the boolean to an integer will be 1 day.
    '''

    def find_files_by_mtime(self, path):
        mtime = int(self.get_option("log_days"))
        cmd = 'find ' + path + ' -type f -mtime -' + str(mtime)
        result = self.call_ext_prog(cmd)
        for line in result['output'].splitlines():
            self.add_copy_spec(line)
